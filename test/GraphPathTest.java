import com.simplegraph.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class GraphPathTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    public void undirectedGraphTest() {

        UndirectedGraph<String> graph  = new UndirectedGraph<>();

        String v1 = "v1";
        String v2 = "v2";
        String v3 = "v3";
        String v4 = "v4";

        //todo
        graph.addVertex(v1);
        graph.addVertex(v2);
        graph.addVertex(v3);
        graph.addVertex(v4);
        graph.addEdge(v1, v2);
        graph.addEdge(v2, v4);
        graph.addEdge(v4, v3);

        assertEquals(1, graph.getAdjacentVertices(v1).size());
        assertEquals(2, graph.getAdjacentVertices(v2).size());
        assertEquals(1, graph.getAdjacentVertices(v3).size());
        assertEquals(2, graph.getAdjacentVertices(v4).size());

        graph.removeEdge(v2, v4);
        graph.addEdge(v2, v3);

        assertEquals(1, graph.getAdjacentVertices(v1).size());
        assertEquals(2, graph.getAdjacentVertices(v2).size());
        assertEquals(2, graph.getAdjacentVertices(v3).size());
        assertEquals(1, graph.getAdjacentVertices(v4).size());

        graph.removeVertex(v2);

        assertEquals(0, graph.getAdjacentVertices(v1).size());
        assertEquals(1, graph.getAdjacentVertices(v3).size());
        assertEquals(1, graph.getAdjacentVertices(v4).size());
    }

    @Test
    public void directedGraphTest() {

        DirectedGraph<String> graph  = new DirectedGraph<>();

        String v1 = "v1";
        String v2 = "v2";
        String v3 = "v3";
        String v4 = "v4";

        //todo
        graph.addVertex(v1);
        graph.addVertex(v2);
        graph.addVertex(v3);
        graph.addVertex(v4);
        graph.addEdge(v1, v2);
        graph.addEdge(v2, v4);
        graph.addEdge(v4, v3);

        assertEquals(1, graph.getAdjacentVertices(v1).size());
        assertEquals(1, graph.getAdjacentVertices(v2).size());
        assertEquals(0, graph.getAdjacentVertices(v3).size());
        assertEquals(1, graph.getAdjacentVertices(v4).size());

        graph.removeEdge(v2, v4);
        graph.addEdge(v2, v3);
        graph.addEdge(v3, v4);

        assertEquals(1, graph.getAdjacentVertices(v1).size());
        assertEquals(1, graph.getAdjacentVertices(v2).size());
        assertEquals(1, graph.getAdjacentVertices(v3).size());
        assertEquals(1, graph.getAdjacentVertices(v4).size());

        graph.removeVertex(v2);
        graph.addEdge(v4, v1);

        assertEquals(0, graph.getAdjacentVertices(v1).size());
        assertEquals(1, graph.getAdjacentVertices(v3).size());
        assertEquals(2, graph.getAdjacentVertices(v4).size());
    }

    @Test
    public void undirectedGraphCustomEdgeTest() {

        UndirectedCustomEdgeGraph<Vertex, Integer> graph  = new UndirectedCustomEdgeGraph<>(() -> 1);

        Vertex v1 = new Vertex("v1");
        Vertex v2 = new Vertex("v2");
        Vertex v3 = new Vertex("v3");
        Vertex v4 = new Vertex("v4");

        //todo
        graph.addVertex(v1);
        graph.addVertex(v2);
        graph.addVertex(v3);
        graph.addVertex(v4);
        graph.addEdge(v1, v2);
        graph.addEdge(v2, v4);
        graph.addEdge(v4, v3);

        graph.removeEdge(v2, v4);
        graph.addEdge(v2, v3, 3);

        assertEquals(1, graph.getAdjacentVertices(v1).size());
        assertEquals(2, graph.getAdjacentVertices(v2).size());
        assertEquals(2, graph.getAdjacentVertices(v3).size());
        assertEquals(1, graph.getAdjacentVertices(v4).size());

        assertEquals(3, (int)graph.getEdge(v3, v2));

        graph.removeVertex(v2);

        assertEquals(0, graph.getAdjacentVertices(v1).size());
        assertEquals(1, graph.getAdjacentVertices(v3).size());
        assertEquals(1, graph.getAdjacentVertices(v4).size());
    }

    @Test
    public void undirectedGraphPathTest() {

        UndirectedGraph<Integer> graph  = new UndirectedGraph<>();

        //todo
        graph.addVertex(1);
        graph.addVertex(2);
        graph.addVertex(3);
        graph.addEdge(1,2);
        graph.addEdge(2,3);

        PathFinder<Integer> pathFinder = new SimplePathFinder<>(graph);

        List<Integer> path = pathFinder.getPath(1,3);

        assertEquals(3, path.size());

        System.out.println(String.join(" -> ", path.stream().map(Object::toString).collect(Collectors.toList())));

        path = pathFinder.getPath(3,1);

        assertEquals(3, path.size());

        System.out.println(String.join(" -> ", path.stream().map(Object::toString).collect(Collectors.toList())));
    }

    @Test
    public void directedGraphPathTest() {

        DirectedGraph<Integer> graph  = new DirectedGraph<>();

        graph.addVertex(1);
        graph.addVertex(2);
        graph.addVertex(3);
        graph.addEdge(1,2);
        graph.addEdge(2,3);

        PathFinder<Integer> pathFinder = new SimplePathFinder<>(graph);

        List<Integer> path = pathFinder.getPath(1,3);

        assertEquals(3, path.size());

        System.out.println(String.join(" -> ", path.stream().map(Object::toString).collect(Collectors.toList())));

        path = pathFinder.getPath(3,1);

        assertEquals(0, path.size());

        System.out.println(String.join(" -> ", path.stream().map(Object::toString).collect(Collectors.toList())));
    }

    @Test
    public void undirectedGraphCustomVertexPathTest() {

        UndirectedGraph<Vertex> graph  = new UndirectedGraph<>();

        Vertex v1 = new Vertex("v1");
        Vertex v2 = new Vertex("v2");
        Vertex v3 = new Vertex("v3");
        Vertex v4 = new Vertex("v4");

        //todo
        graph.addVertex(v1);
        graph.addVertex(v2);
        graph.addVertex(v3);
        graph.addVertex(v4);
        graph.addEdge(v1, v2);
        graph.addEdge(v2, v4);
        graph.addEdge(v4, v3);

        PathFinder<Vertex> pathFinder = new SimplePathFinder<>(graph);

        List<Vertex> path = pathFinder.getPath(v1, v4);

        assertEquals(3, path.size());

        System.out.println(String.join(" -> ", path.stream().map(Object::toString).collect(Collectors.toList())));

        path = pathFinder.getPath(v3, v1);

        assertEquals(4, path.size());

        System.out.println(String.join(" -> ", path.stream().map(Object::toString).collect(Collectors.toList())));
    }
}