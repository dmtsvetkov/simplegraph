package com.simplegraph;

import java.util.*;

public class UndirectedGraph<TVertex> implements Graph<TVertex> {

    private Map<TVertex, Set<TVertex>> vertices = new HashMap<>();

    @Override
    public void addVertex(TVertex v) {
        vertices.put(v, new HashSet<>());
    }

    @Override
    public void removeVertex(TVertex v) {
        for (TVertex adj : getAdjacentVertices(v)) {
            vertices.get(adj).remove(v);
        }
        vertices.remove(v);
    }

    @Override
    public void addEdge(TVertex v1, TVertex v2) {
        //todo add vertex if absent?
        vertices.get(v1).add(v2);
        vertices.get(v2).add(v1);
    }

    @Override
    public void removeEdge(TVertex v1, TVertex v2) {
        vertices.get(v1).remove(v2);
        vertices.get(v2).remove(v1);
    }

    @Override
    public Set<TVertex> getAdjacentVertices(TVertex v) {
        return vertices.get(v);
    }
}
