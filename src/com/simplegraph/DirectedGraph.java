package com.simplegraph;

import java.util.*;

public class DirectedGraph<TVertex> implements Graph<TVertex> {

    private Map<TVertex, Set<TVertex>> vertices = new HashMap<>();

    @Override
    public void addVertex(TVertex v) {
        vertices.put(v, new HashSet<>());
    }

    @Override
    public void removeVertex(TVertex v) {
        for (TVertex adj : vertices.keySet()) {
            vertices.get(adj).remove(v);
        }
        vertices.remove(v);
    }

    @Override
    public void addEdge(TVertex v1, TVertex v2) {
        //todo add vertex if absent?
        vertices.get(v1).add(v2);
    }

    @Override
    public void removeEdge(TVertex v1, TVertex v2) {
        vertices.get(v1).remove(v2);
    }

    @Override
    public Set<TVertex> getAdjacentVertices(TVertex v) {
        return vertices.get(v);
    }
}
