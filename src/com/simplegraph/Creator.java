package com.simplegraph;

public interface Creator<T> {
    T create();
}
