package com.simplegraph;

import java.util.*;

public class SimplePathFinder<TVertex> implements PathFinder<TVertex> {

    private Graph<TVertex> graph;


    public SimplePathFinder(Graph<TVertex> graph) {
        this.graph = graph;
    }

    @Override
    public List<TVertex> getPath(TVertex v1, TVertex v2) {

        Map<TVertex,TVertex> path = new HashMap<>();
        Queue<TVertex> queue = new LinkedList<>();
        Set<TVertex> visited = new HashSet<>();

        queue.add(v1);
        visited.add(v1);
        path.put(v1, null);

        while (!queue.isEmpty()) {
            TVertex v = queue.remove();
            if (v.equals(v2)) {
                List<TVertex> result = new ArrayList<>();
                result.add(v);
                TVertex previous = v;
                do {
                    previous = path.get(previous);
                    result.add(previous);
                } while (!v1.equals(previous));

                Collections.reverse(result);
                return result;
            }

            Set<TVertex> adjacents = graph.getAdjacentVertices(v);
            for (TVertex adj : adjacents) {
                if (!visited.contains(adj)) {
                    queue.add(adj);
                    path.put(adj, v);
                    visited.add(adj);
                }
            }

        }

        return new ArrayList<>();
    }
}
