package com.simplegraph;

public interface CustomEdgeGraph<TVertex, TEdge> extends Graph<TVertex> {
    void addEdge(TVertex v1, TVertex v2, TEdge e);
    TEdge getEdge(TVertex v1, TVertex v2);
}
