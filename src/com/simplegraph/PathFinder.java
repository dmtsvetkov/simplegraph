package com.simplegraph;

import java.util.List;

public interface PathFinder<TVertex> {
    List<TVertex> getPath(TVertex v1, TVertex v2);
}
