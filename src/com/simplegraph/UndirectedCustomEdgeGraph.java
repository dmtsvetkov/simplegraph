package com.simplegraph;

import java.util.*;

public class UndirectedCustomEdgeGraph<TVertex, TEdge> implements CustomEdgeGraph<TVertex, TEdge> {

    private Map<TVertex, Map<TVertex, TEdge>> vertices = new HashMap<>();
    private Creator<TEdge> edgeCreator;

    public UndirectedCustomEdgeGraph(Creator<TEdge> edgeCreator) {
        this.edgeCreator = edgeCreator;
    }

    @Override
    public void addVertex(TVertex v) {
        vertices.put(v, new HashMap<>());
    }

    @Override
    public void removeVertex(TVertex v) {
        for (TVertex adj : getAdjacentVertices(v)) {
            vertices.get(adj).remove(v);
        }
        vertices.remove(v);
    }

    @Override
    public void addEdge(TVertex v1, TVertex v2) {
        TEdge e = edgeCreator.create();
        vertices.get(v1).put(v2, e);
        vertices.get(v2).put(v1, e);
    }

    @Override
    public void removeEdge(TVertex v1, TVertex v2) {
        vertices.get(v1).remove(v2);
        vertices.get(v2).remove(v1);
    }

    @Override
    public Set<TVertex> getAdjacentVertices(TVertex v) {
        return vertices.get(v).keySet();
    }

    @Override
    public void addEdge(TVertex v1, TVertex v2, TEdge e) {
        vertices.get(v1).put(v2, e);
        vertices.get(v2).put(v1, e);
    }

    @Override
    public TEdge getEdge(TVertex v1, TVertex v2) {
        return vertices.get(v1).get(v2);
    }
}
