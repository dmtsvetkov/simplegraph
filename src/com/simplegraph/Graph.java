package com.simplegraph;

import java.util.Set;

public interface Graph<TVertex> {
    void addVertex(TVertex v);
    void removeVertex(TVertex v);
    void addEdge(TVertex v1, TVertex v2);
    void removeEdge(TVertex v1, TVertex v2);

    Set<TVertex> getAdjacentVertices(TVertex v);
    //List<TVertex> getVertices();
}
